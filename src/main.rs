mod random_sequence;

fn main() {
    let mut r = random_sequence::RandomSequenceOfUnique::new(0, 1);
    for _ in 0..20 {
        println!("{}", r.next());
    }
    for c in 0..20 {
        println!("{}: {}", c, r.by_index(c));
    }
}